
class Solution {
	public:
		double pow(double x, int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(n==0) return 1;
			if(x==0) return 0;

			if(n==1) return x;

			bool neg=false;
			if(n<0) {neg=true; n=-n;}

			//naive
			/*
				 double res=1;
				 for(int i=0; i<n; i++)
				 res *= x;
			 */

			//recursion
			//double res=pow(x, n/2)*pow(x, n-n/2);

			double res=pow(x, n/2);

			if(n%2==0)
				res = res*res;
			else
				res = res*res*x;

			if(neg) res = 1/res;
			return res;
		}
};
