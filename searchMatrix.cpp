
class Solution {
	public:
		bool searchMatrix(vector<vector<int> > &matrix, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(matrix.size()==0 || matrix[0].size()==0) 
				return false;

			int nRows=matrix.size();
			int nCols=matrix[0].size();

			int low=0, high=nRows*nCols-1;

			while(low<=high)
			{
				int mid=(low+high)/2;

				int tmp=matrix[mid/nCols][mid%nCols];

				if(tmp==target)
					return true;
				else if(tmp<target)
					low=mid+1;
				else
					high=mid-1;   
			}

			return false;
		}
};


class Solution_Recursion {
	public:
		bool searchMatrix(vector<vector<int> > &matrix, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(matrix.size()==0 || matrix[0].size()==0) 
				return false;

			int nRows=matrix.size();
			int nCols=matrix[0].size();

			// Recursion
			return searchRec(matrix, 0, nCols*nRows-1, target, nCols);
		}

		bool searchRec(vector<vector<int> > &matrix, int low, int high, 
				int target, int nCols)
		{
			if(low>high) 
				return false;

			int mid=(low+high)/2;
			int tmp=matrix[mid/nCols][mid%nCols];

			if(tmp==target)
				return true;
			else if(tmp<target)
				return searchRec(matrix, mid+1, high, target, nCols);
			else 
				return searchRec(matrix, low, mid-1, target, nCols);
		}
};
