#include<iostream>
#include<vector>
#include<string>

using namespace std;

class Solution {
	public:
		vector<vector<string> > solveNQueens(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			vector<int> aAns(n, -1);
			vector<vector<string> > ans;

			solveQueensRec(ans, aAns, n, 0);

			return ans;
		}

		void solveQueensRec(vector<vector<string> > &ans, vector<int> &aAns, 
				int n, int row)
		{
			if(row==n)
			{
				vector<string> str_ans;
				for(int i=0; i<n; i++)
				{
					string str="";
					for(int j=0; j<n; j++)
					{
						if(aAns[i]==j)
							str+='Q';
						else
							str+='.';
					}
					str_ans.push_back(str);
				}
				ans.push_back(str_ans);
				return;
			}

			for(int col=0; col<n; col++)
			{
				if( isValid(aAns, row, col) )
				{
					aAns[row]=col;
					solveQueensRec(ans, aAns, n, row+1);
				}
			}
		}

		bool isValid(vector<int> &aAns, int row, int col)
		{
			for(int row1=0; row1<row; row1++)
			{
				int col1 = aAns[row1];
				if(col1==col)
					return false;

				int dist_col = col-col1;
				if(dist_col<0) dist_col=-dist_col;
				int dist_row = row-row1;

				if(dist_col==dist_row)
					return false;
			}
			return true;
		}
};

int main()
{
	Solution sol;

	sol.solveNQueens(2);
	return 0;
}



