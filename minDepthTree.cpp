/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		int minDepth(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			return minDepth_Rec(root);
		}

		int minDepth_Rec(TreeNode *root)
		{
			if(!root) return 0;

			if(!root->left && !root->right) return 1;

			int min_left = minDepth_Rec(root->left);
			int min_right = minDepth_Rec(root->right);

			if(root->left && root->right) 
				return 1+min(min_left, min_right);

			return (root->left? min_left+1 : min_right+1);
		}
};
