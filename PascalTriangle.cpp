
//Actually, it is C(n,i)
class Solution {
	public:
		vector<int> getRow(int rowIndex) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<int> ans(rowIndex+1, 1);

			for(int i=2; i<=rowIndex; i++)
				for(int j=i-1;j>=1;j--)
					ans[j] += ans[j-1];

			return ans;
		}
};
