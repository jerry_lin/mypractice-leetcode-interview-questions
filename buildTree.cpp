/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int n1=preorder.size(), n2=inorder.size();
			if(n1!=n2) return NULL;

			return buildTree_Rec(preorder, 0, n1-1, inorder, 0, n2-1);
		}

		TreeNode *buildTree_Rec(vector<int> &preorder, int low1, int high1, 
				vector<int> &inorder, int low2, int high2)
		{
			if(low1>high1) return NULL;

			int target = preorder[low1];

			int pos = find(inorder, low2, high2, target);

			TreeNode *node = new TreeNode(target);
			TreeNode *left=buildTree_Rec(preorder, low1+1, low1+(pos-low2), 
					inorder, low2, pos-1);
			TreeNode *right=buildTree_Rec(preorder, low1+(pos-low2)+1, high1,
					inorder, pos+1, high2);

			node->left=left;
			node->right=right;
			return node;
		}

		int find(vector<int> inorder, int low, int high, int val)
		{
			for(int i=low; i<=high; i++)
				if(inorder[i]==val)
					return i;

			return INT_MIN;
		}
};
