
class Solution {
	public:
		int longestConsecutive(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(num.size()==0) return 0;
			unordered_set<int> numSet;

			for(int i=0; i<num.size(); i++)
				numSet.insert(num[i]);

			int max=0;
			while(!numSet.empty())
			{
				int curr=(*numSet.begin());

				int count=0;
				int tmp=curr;
				while(numSet.count(tmp))
				{
					numSet.erase(tmp);
					tmp-=1;
					count++;
				}
				tmp=curr+1;
				while(numSet.count(tmp))
				{
					numSet.erase(tmp);
					tmp+=1;
					count++;
				}

				if(count>max) max=count;
			}

			return max;
		}
};
