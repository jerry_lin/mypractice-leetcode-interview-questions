
class Solution {
	public:
		int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int m=obstacleGrid.size();

			if(m==0) return 0;

			int n=obstacleGrid[0].size();
			if(obstacleGrid[m-1][n-1]==1) return 0;

			vector<int> count(n, 1);

			int pos=0;
			while(pos<n)
			{
				if(obstacleGrid[0][pos]==1) break;
				pos++;
			}
			while(pos<n)
				count[pos++]=0;

			for(int i=1; i<m; i++)
			{
				if(obstacleGrid[i-1][0]==1)
					count[0]=0;
				for(int j=1; j<n; j++)
				{
					if(obstacleGrid[i-1][j]==1) count[j]=0;
					if(obstacleGrid[i][j-1]!=1) count[j]+=count[j-1];
				}
			}

			return count[n-1];
		}
};
