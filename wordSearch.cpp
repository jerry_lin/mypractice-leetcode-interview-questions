#include<iostream>
#include<algorithm>
#include<vector>
#include<string>

using namespace std;

class Solution {
	public:
		bool exist(vector<vector<char> > &board, string word) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(word.size()==0) return true;
			if(board.size()==0 || board[0].size()==0) return false;
			int row=board.size(), col=board[0].size();

			vector<bool> entry(col, false);
			vector<vector<bool> > used(row, entry);

			for(int i=0; i<row; i++)
				for(int j=0; j<col; j++)
				{
					if(exist_rec(board, used, word, 0, i, j))
						return true;
				}            
			return false;
		}

		//Recursion
		bool exist_rec(vector<vector<char> > &board, vector<vector<bool> > &used, 
				string word, int pos, int row, int col)
		{
			if(pos==word.size())
				return true;

			if(row<0 || row>=board.size() || col<0 || col>=board[0].size()) 
				return false;

			if(used[row][col] || board[row][col] != word[pos])
				return false;

			used[row][col]=true;

			if(exist_rec(board, used, word, pos+1, row-1, col)
					|| exist_rec(board, used, word, pos+1, row+1, col)
					|| exist_rec(board, used, word, pos+1, row, col-1)
					|| exist_rec(board, used, word, pos+1, row, col+1)
				)
				return true;
			else
			{
				used[row][col]=false;
				return false;
			}
		}
};

int main()
{
	Solution sol;

	return 0;
}
