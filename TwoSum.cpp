class Solution {
	public:
		vector<int> twoSum(vector<int> &numbers, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int index1, index2;
			vector<int> res;
			vector<pair<int, int>> table;

			for(int i=0; i<numbers.size(); i++)
				table.push_back(pair<int, int>(numbers[i],i+1));

			sort(table.begin(), table.end());

			int start=0, end=numbers.size()-1;

			while(start<end)
			{
				int sum=table[start].first+table[end].first;
				if(sum==target)
				{
					index1=table[start].second;
					index2=table[end].second;

					if(index1>index2)
					{
						int tmp=index1;
						index1=index2;
						index2=tmp;
					}

					res.push_back(index1);
					res.push_back(index2);
					return res;
					//cout<<"index1="<<index1<<", index2="<<index2;
				}
				else if(sum<target)
					start++;
				else
					end--;
			}
		}
};
