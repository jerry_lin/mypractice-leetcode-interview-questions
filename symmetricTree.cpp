/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		bool isSymmetric(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!root) return true;

			return isSymmectric_Rec(root->left, root->right);
		}

		bool isSymmectric_Rec(TreeNode *left, TreeNode *right)
		{
			if(left&&!right || !left&&right) return false;

			if(!left && !right) return true;

			if(left->val == right->val)
				return (isSymmectric_Rec(left->left, right->right)
						&& isSymmectric_Rec(left->right, right->left));
			else
				return false;
		}
};
