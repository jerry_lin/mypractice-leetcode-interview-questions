
class Solution {
	public:
		vector<vector<int> > combinationSum2(vector<int> &candidates, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			vector<int> aAns;

			if(candidates.size()==0) return ans;

			sort(candidates.begin(), candidates.end());

			combinationSum_Rec(candidates, target, 0, aAns, ans);

			return ans;
		}

		void combinationSum_Rec(vector<int> &candidates, int target, int pos, 
				vector<int> aAns, vector<vector<int> > &ans)
		{
			if(pos>=candidates.size() || target < candidates[pos]) return;

			//not include candidates[pos]
			int next_pos=pos+1;
			//skip the same elements
			while(next_pos<candidates.size() && candidates[next_pos]==candidates[pos])
				next_pos++;
			if(next_pos<candidates.size())
				combinationSum_Rec(candidates, target, next_pos, aAns, ans);

			//include candidates[pos]
			aAns.push_back(candidates[pos]);
			if(target==candidates[pos])
				ans.push_back(aAns);
			else
				combinationSum_Rec(candidates, target-candidates[pos], pos+1, aAns, ans); 
		}
};
