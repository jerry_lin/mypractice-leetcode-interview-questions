
class Solution {
	public:
		string countAndSay(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			char buffer[32];

			string ans="";
			if(n==0) return ans;

			string s ="1";
			ans="1";

			for(int i=1;i<n;i++)
			{
				s=ans;
				ans.clear();

				int pos=0;
				while(pos<s.size())
				{
					char target=s[pos];
					int count=0;
					while(pos<s.size() && s[pos]==target)
					{
						count++; 
						pos++;
					}
					stringstream ss;
					ss << count << target;
					ans = ans+ss.str();
				}
			}       
			return ans;
		}
};
