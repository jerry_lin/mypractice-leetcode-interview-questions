class Solution {
	public:
		int maxSubArray(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int max=A[0], sum=0;
			int idx=0;

			while(idx<n)
			{
				sum+=A[idx];

				if(sum>max) max=sum;

				if(sum<0) sum=0;

				idx++;
			}

			return max;
		}
};

class Solution2 {
	public:
		int maxSubArray(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int ret = A[0], sum = 0;
			for (int i = 0; i < n; ++i) {
				sum = max(sum + A[i], A[i]);
				ret = max(ret, sum);
			}
			return ret;
		}
};
