
//Word Ladder
class Solution {
	public:
		int ladderLength(string start, string end, unordered_set<string> &dict) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(dict.empty()) return 0;
			int strLen = (*(dict.begin())).size();
			if(start.size()!=end.size() || start.size() != strLen) return 0;
			if(start==end) return 1;

			int shortestLen=1;
			queue<string> curr_queue;
			unordered_map<string, bool> visited;

			curr_queue.push(start);
			visited[start]=true;

			while(!curr_queue.empty())
			{
				shortestLen++;
				queue<string> new_queue;

				while(!curr_queue.empty())
				{
					string curr=curr_queue.front();
					curr_queue.pop();
					for(int pos=0; pos<strLen; pos++) //iterate all positions in the word
					{
						for(char c='a'; c<='z'; c++)    //iterate all possible chars
						{
							if(c==curr[pos]) continue;

							string newWord=curr;
							newWord.replace(pos,1,1,c);

							if(newWord==end) return shortestLen;

							if(dict.count(newWord)==0) continue; //not in dict
							if(visited.count(newWord)) continue; //already visited

							new_queue.push(newWord);
							visited[newWord]=true;
						}//for a-z
					}//for pos
				}//while
				curr_queue = new_queue; //proceed to the next depth/level
			}
			return 0;
		}
};


class Solution2 {
	public:
		int ladderLength(string start, string end, unordered_set<string> &dict) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(dict.empty()) return 0;
			int strLen = (*(dict.begin())).size();
			if(start.size()!=end.size() || start.size() != strLen) return 0;
			if(start==end) return 1;

			int shortestLen=1;
			queue<string> str_queue;
			unordered_map<string, bool> visited;

			str_queue.push(start);
			visited[start]=true;
			int currLevelCount=1, nextLevelCount=0;

			while(!str_queue.empty())
			{
				if(currLevelCount==0)
				{
					currLevelCount=nextLevelCount;
					nextLevelCount=0;
					shortestLen++;
				}

				string curr=str_queue.front();
				str_queue.pop();
				currLevelCount--;

				for(int pos=0; pos<strLen; pos++)
				{
					for(char c='a'; c<='z'; c++)
					{
						if(c==curr[pos]) continue;

						string newWord=curr;
						newWord.replace(pos,1,1,c);

						if(newWord==end) return shortestLen+1;

						if(dict.count(newWord)==0) continue; //not in dict

						if(visited.count(newWord)) continue; //already visited

						str_queue.push(newWord);
						visited[newWord]=true;
						nextLevelCount++;
					}
				}
			}
			return 0;
		}
};
