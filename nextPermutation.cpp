class Solution {
	public:
		void nextPermutation(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int sz=num.size();

			int idx=sz-1;

			while(--idx>=0 && num[idx]>=num[idx+1]);

			if(idx>=0)
			{
				int idx2=sz-1;
				while(num[idx2]<=num[idx]) idx2--;
				swap(num[idx2], num[idx]);
				reverse( num.begin()+idx+1, num.end() );
			}
			else
				reverse( num.begin(), num.end() );
		}
};
