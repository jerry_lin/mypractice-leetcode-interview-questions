#include<iostream>
#include<vector>
#include<queue>

//http://www.mitbbs.com/article_t/JobHunting/32339819.html
//refer to http://blog.sina.com.cn/s/blog_b9285de20101j9a7.html

using namespace std;

class Cell
{
	public:
		int x;
		int y;
		int height;

	public:
		Cell(int x, int y, int height):x(x),y(y),height(height){}
		int getHeight() const {return height;}
		int getWaterHeight(int h) {return max(h - height, 0);}
};


class compare
{
	public:
		bool operator()(const Cell a, const Cell b) const
		{
			return (a.getHeight()>b.getHeight());
		}
};


class WaterTrap2D
{
	vector<vector<Cell> >matrix;
	int row;
	int col;
	priority_queue<Cell, vector<Cell>, compare> pq; //should be local
	vector<vector<bool> > visited; //local 
	int volume;

	public:
	WaterTrap2D()
	{
		//init matrix, row, col
		//init visited = false
		volume=0;
	} 

	int calVolume()
	{
		for(int i=0; i<row; i++)
		{
			pq.push(matrix[i][0]);
			visited[i][0]=true;
			pq.push(matrix[i][col-1]);
			visited[i][col-1]=true;
		}
		for(int i=0; i<col; i++)
		{
			pq.push(matrix[0][i]);
			visited[0][i]=true;
			pq.push(matrix[row-1][i]);
			visited[row-1][i]=true;
		}

		while(!pq.empty())
		{
			Cell top=pq.top();
			pq.pop();
			int x=top.x;
			int y=top.y;
			int height=top.height;

			//visit neighbors of top
			visitNeighbor(x-1, y, height);
			visitNeighbor(x+1, y, height);
			visitNeighbor(x, y-1, height);
			visitNeighbor(x, y+1, height);
		}
		return volume;
	}

	void visitNeighbor(int x, int y, int currHeight)
	{ 
		if(visited[x][y] || x<0 || x>=row || y<0 || y>=col) return;

		if(matrix[x][y].getHeight()<currHeight)
			volume += matrix[x][y].getWaterHeight(currHeight);

		Cell aNeighbor(x, y, max(currHeight, matrix[x][y].getHeight() ) );
		pq.push(aNeighbor);

		visited[x][y]=true;
	}

};


int main()
{
	WaterTrap2D sol;

	return 0;
}
