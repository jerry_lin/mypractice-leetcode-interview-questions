
class Solution {
	public:
		int lengthOfLastWord(const char *s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int count=0;
			const char *p=s;
			while(*p!='\0' && *p==' ') p++;

			while(*p!='\0')
			{
				count=0;
				while(*p!='\0' && *p!=' ') {count++; p++;}
				while(*p!='\0' && *p==' ') p++;
			}
			return count;
		}
};
