
class Solution {
	public:
		vector<string> fullJustify(vector<string> &words, int L) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len = words.size();
			vector<string> ans;
			if(len==0) return ans;

			vector<int> countVec;
			vector<int> lenVec;

			int i=0, wordLen=0, count=0;
			while(i<len)
			{
				if(wordLen+words[i].size()<=L)
				{
					wordLen += words[i].size()+1;//at least 1 space
					count++;
					i++;
				}
				else
				{
					countVec.push_back(count);
					lenVec.push_back(wordLen-count);
					count=0; wordLen=0;
				}
			}
			if(count!=0) // last line
			{
				countVec.push_back(count);
				lenVec.push_back(wordLen-count);
			}

			int nLine=countVec.size();

			int pos=0;
			for(i=0; i<nLine-1; i++)
			{
				if(countVec[i]==1)
				{
					string line=words[pos];
					for(int j=0; j<L-lenVec[i]; j++)
						line += " ";
					ans.push_back(line);
					pos+=countVec[i];
				}
				else
				{
					int nSpace=L-lenVec[i];
					int avg=nSpace/(countVec[i]-1);
					int rem=nSpace%(countVec[i]-1);

					string line=words[pos++];
					for(int j=0; j<countVec[i]-1; j++)
					{
						if(j<rem) 
						{
							for(int k=0; k<avg+1; k++) line += " ";
							line += words[pos++];
						}
						else
						{
							for(int k=0; k<avg; k++) line += " ";
							line += words[pos++];
						}
					}
					ans.push_back(line);
				}
			}
			//for last line
			string line=words[pos++];
			for(int j=0; j<countVec[nLine-1]-1; j++)
				line += " "+words[pos++];
			while(line.size()<L) line += " ";

			ans.push_back(line);

			return ans;
		}
};
