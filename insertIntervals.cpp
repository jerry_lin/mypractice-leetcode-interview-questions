/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
	public:
		vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int len=intervals.size();
			vector<Interval> ans;
			if(len==0) 
			{
				ans.push_back(newInterval);
				return ans;
			}

			int i=0;
			while(i<len)
			{
				if(newInterval.start>intervals[i].end)
				{
					ans.push_back(intervals[i]);
					i++;
					continue;
				}

				if(newInterval.end<intervals[i].start)
				{
					ans.push_back(newInterval);
					while(i<len) ans.push_back(intervals[i++]);
					return ans;
				}
				else
				{
					newInterval.start=min(newInterval.start, intervals[i].start);
					newInterval.end=max(newInterval.end, intervals[i].end);
					i++;
				}
			}
			ans.push_back(newInterval);
			return ans;
		}
};


/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
	public:

		static bool mycompare(const Interval &a, const Interval &b)
		{
			return (a.start<b.start);
		}

		vector<Interval> insert(vector<Interval> &intervals, Interval newInterval) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			intervals.push_back(newInterval);
			if(intervals.size()<=1) return intervals;

			vector<Interval> ans;
			Interval tmp;

			sort(intervals.begin(), intervals.end(), Solution::mycompare);

			ans.push_back(intervals[0]);
			for(int i=1; i<intervals.size(); i++)
			{
				if(intervals[i].start <= ans.back().end)
					ans.back().end=max(intervals[i].end, ans.back().end);
				else
					ans.push_back(intervals[i]);
			}

			return ans;
		}
};
