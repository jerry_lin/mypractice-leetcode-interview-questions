class Solution {
	public:
		vector<vector<int> > permuteUnique(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			if(num.size()<=0) return ans;

			vector<int> s;
			vector<bool> used(num.size(), false);

			sort(num.begin(), num.end());
			permuteUniqueRec(num, s, used, ans);

			return ans;
		}

		void permuteUniqueRec(vector<int> &num, vector<int> &s, vector<bool> &used,
				vector<vector<int> > &ans)
		{
			if(s.size() == num.size())
			{
				ans.push_back(s);
				return;
			}

			for(int i=0; i<used.size(); i++)
			{
				if(!used[i] )
				{
					if(i!=0 && num[i]==num[i-1] && used[i-1]) 
						continue;

					used[i]=true;
					s.push_back(num[i]);
					permuteUniqueRec(num, s, used, ans);
					s.pop_back();
					used[i]=false;
				}

			}

		}
};
