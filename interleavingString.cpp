


class Solution {
	public:
		bool isInterleave(string s1, string s2, string s3) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function          
			int len1=s1.size(), len2=s2.size(), len3=s3.size();        
			if(len1+len2 != len3) return false;

			//switch to save space later on.
			if(len2>len1)  {string tmp=s2; s2=s1; s1=tmp;}

			//isPrefix(i,j) indicates s1.substr(0, i)+s2.substr(0,j)==s3.substr(0,i+j);
			//isPrefix(i,j) = isPrefix(i-1,j)&&s1[i-1]==s3[i+j-1] || isPrefix(i,j-1)&&s2[j-1]==s3[i+j-1]
			//Here, only an array is used to simplify the space overhead (O(min(len1, len2)))
			vector<bool> isPrefix(s2.size()+1, false);
			isPrefix[0]=true;

			for(int i=1; i<=s2.size(); i++) isPrefix[i] = s2.substr(0, i) == s3.substr(0, i);

			for(int i=1; i<=s1.size(); i++){
				isPrefix[0] = s1.substr(0, i)==s3.substr(0,i);
				for(int j=1; j<=s2.size(); j++)
					isPrefix[j]= (isPrefix[j-1]&&s2[j-1]==s3[i+j-1] || isPrefix[j]&&s1[i-1]==s3[i+j-1]);
			}
			return isPrefix.back();
		}
};



class Solution_Rec {
	public:
		bool isInterleave(string s1, string s2, string s3) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function    

			if(s1.size()+s2.size() != s3.size()) return false;
			return isInterleave_Rec(s1, s2, s3);
		}

		bool isInterleave_Rec(string s1, string s2, string s3)
		{
			if(s1.size()==0 && s2.size()==0 && s3.size()==0)
				return true;

			if(s1[0]==s3[0] && s2[0]==s3[0])
				return (isInterleave_Rec(s1.substr(1), s2, s3.substr(1)) 
						|| isInterleave_Rec(s1, s2.substr(1), s3.substr(1)) );
			if(s1[0]==s3[0])
				return isInterleave_Rec(s1.substr(1), s2, s3.substr(1));
			if(s2[0]==s3[0])
				return isInterleave_Rec(s1, s2.substr(1), s3.substr(1));
			return false;
		}
};
