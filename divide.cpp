#include<iostream>
#include<cassert>

using namespace std;

class Solution {
	public:
		int divide(int dividend, int divisor) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			assert(divisor != 0);

			int sign=0;
			if(dividend<0 && divisor>0 || dividend>0&&divisor<0) sign=1;

			//(INT_MIN)* (-1) is also handled !!! Because unsigned int can hold it.
			unsigned int dividend_u = 
				dividend>0? (unsigned int)dividend : (unsigned int) ((dividend^-1)+1);
			unsigned int divisor_u =
				divisor>0? (unsigned int)divisor : (unsigned int) ((divisor^-1)+1);

			//cout<<hex<<dividend_u<< "  "<<divisor_u << endl;

			int res=divide_unsigned(dividend_u, divisor_u);
			if(sign) res=(res^-1)+1;
			return res;
		}

		int divide_unsigned(unsigned int dividend, unsigned int divisor)
		{
			if(dividend<divisor) return 0;
			if(dividend==divisor) return 1;

			int count=1;
			unsigned int tmp=divisor;

			//if use <=, INT_MIN will be a problem!!
			//WHY? Because tmp will be out of boundary and becomes 0 !!
			while( (tmp<<1) < dividend ) 			
			{
				tmp <<= 1;
				count <<= 1;
			}           
			return count + divide_unsigned(dividend-tmp, divisor);
		}
};

int main()
{
	Solution sol;

	int a=-2147483648;
	int b=1;
	//int b=-2147483648;
	int res = sol.divide(a,b);

	cout << res << endl;

	cout<<hex<<a<<endl;

	return 0;
}
