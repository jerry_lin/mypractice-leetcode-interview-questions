#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<stack>

using namespace std;

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		vector<int> inorderTraversal(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			vector<int> ans;
			if(!root) return ans;

			stack<pair<TreeNode*, bool> > st;
			st.push(pair<TreeNode*, bool>(root, false));

			while(!st.empty())
			{
				pair<TreeNode*, bool> top=st.top();

				if(top.second==false)//if not explore its left child yet
				{
					st.top().second=true;
					if(top.first->left)
						st.push(pair<TreeNode*, int>(top.first->left,false) );
				}
				else
				{
					ans.push_back(top.first->val);
					TreeNode *tmp=top.first->right;
					st.pop();
					if(tmp) st.push(pair<TreeNode*, int>(tmp, false) );    
				}
			}
			return ans;
		}
};

//another solution without mark
class Solution2 {
	public:
		vector<int> inorderTraversal(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<int> ans;
			stack<TreeNode*> s;

			TreeNode *current = root;

			while (!s.empty() || current) {
				if (current) {
					s.push(current);
					current = current->left;
				}
				else {
					ans.push_back(s.top()->val);
					current = s.top()->right;
					s.pop();
				}
			}
			return ans;
		}
};

int main()
{

	TreeNode* tree = new TreeNode(1);
	Solution sol;

	sol.inorderTraversal(tree);

	return 0;
}
