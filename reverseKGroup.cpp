
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *reverseKGroup(ListNode *head, int k) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!head || !head->next || k<=1) return head;

			int len=0;
			ListNode *p=head;
			while(p){len++; p=p->next;}

			if(len<k) return head;

			p=head;

			for(int i=0; i<k-1; i++)
			{
				ListNode *q=p->next;
				p->next=q->next;
				q->next=head;
				head=q;
			}

			p->next=reverseKGroup(p->next, k);
			return head;
		}

};
