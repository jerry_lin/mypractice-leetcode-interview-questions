
//Median of Two Sorted Arrays
class Solution {
	public:
		double findMedianSortedArrays(int A[], int m, int B[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(m==0 && n==0) return 0;
			if(m==0) return ((B[n/2]+B[(n-1)/2])/2.0);
			if(n==0) return ((A[m/2]+A[(m-1)/2])/2.0);

			return median(A, m, B, n, max(0, (m+n)/2-n), min(m-1, (m+n)/2));
		}

		double median(int A[], int m, int B[], int n, int low, int high)
		{
			if(low>high)
				return median(B, n, A, m, max(0, (m+n)/2-m), 
						min(n-1, (m+n)/2) );
			int i=low+(high-low)/2;
			int j=(m+n)/2-i;

			//assert (i>=0 && i<=m && j>=0 && j<=n)
			int Ai_1 = i==0? INT_MIN: A[i-1];
			int Bj_1 = j==0? INT_MIN: B[j-1];
			int Ai   = i==m? INT_MAX: A[i];
			int Bj   = j==n? INT_MAX: B[j];

			if(Ai<Bj_1) return median(A, m, B, n, i+1, high);
			else if(Ai>Bj) return median(A, m, B, n, low, i-1);

			if((m+n)%2==1) return A[i];
			else return (Ai+ max(Ai_1, Bj_1))/2.0;
		}
};
