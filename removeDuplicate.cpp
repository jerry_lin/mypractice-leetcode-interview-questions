
class Solution {
	public:
		int removeDuplicates(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(n==0) return 0;

			int p=0, q=0;

			while(p<n)
			{
				if(A[p]==A[q]) p++;
				else
				{
					q++;
					A[q]=A[p];
					p++;
				}
			}
			return q+1;
		}
};
