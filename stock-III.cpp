
//Use DP   
// 1. find the max profit for **day[0,i]** in O(n) (dp1[i])             
// 2. find the max profit for **day[i,n-1]** in O(n) (dp2[i])         
// 3. the max profit with two transactions will be **max(dp1[i]+dp2[i])**

class Solution {
	public:
		int maxProfit(vector<int> &prices) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len = prices.size();
			if(len<=1) return 0;

			vector<int> dp1(len, 0), dp2(len, 0);

			//find max profit for day[0, i] (dp1[i])
			int currMin=prices[0];
			for(int i=1; i<len; i++)
			{
				if(prices[i]<currMin) currMin=prices[i];
				dp1[i]=max(dp1[i-1], prices[i]-currMin);
			}

			//find max profit for day[i, n-1] (dp2[i])
			int currMax=prices[len-1];
			for(int i=len-2; i>=0; i--)
			{
				if(prices[i]>currMax) currMax=prices[i];
				dp2[i]=max(dp2[i+1], currMax-prices[i]);
			}

			// merge two transactions
			int ans=0;
			for(int i=0; i<len; i++)   ans=max(ans, dp1[i]+dp2[i]);
			return ans;
		}
};
