/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		TreeNode *sortedArrayToBST(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int nLen=num.size();

			if(nLen==0) return NULL;

			return toBST_Rec(num, 0, nLen-1);
		}

		TreeNode *toBST_Rec(vector<int> &num, int low, int high)
		{
			if(low>high)
				return NULL;

			int mid=low+(high-low)/2;

			TreeNode *newNode = new TreeNode(num[mid]);

			newNode->left = toBST_Rec(num, low, mid-1);
			newNode->right = toBST_Rec(num, mid+1, high);

			return newNode;
		}
};
