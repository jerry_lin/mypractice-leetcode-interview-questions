/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *removeNthFromEnd(ListNode *head, int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			ListNode *target=0, *curr=head;
			ListNode *prev=target;
			int count=0;

			if(head==NULL || n==0) return head;

			while(curr!=NULL && curr->next!=NULL && count < n-1)
			{
				curr=curr->next;
				count++;
			}

			if(count==n-1) 
			{
				prev=target;
				target=head;
			}
			else
				return head;

			while(curr!=NULL && curr->next!=NULL)
			{
				curr=curr->next;
				prev=target;
				target=target->next;
			}

			if(prev)
				prev->next=target->next;
			else
				head=target->next;

			return head;

		}
};

