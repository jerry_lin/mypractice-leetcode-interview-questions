
class Solution {
	public:
		int maxProfit(vector<int> &prices) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len=prices.size();
			if(len==0) return 0;

			int profit=0;

			for(int i=0; i<len-1; i++)
				profit += max(prices[i+1]-prices[i], 0);

			return profit;

		}
};
