class Solution {
	public:
		bool isValid(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			stack<char> st;

			int len=s.length();

			for(int i=0; i<len; i++)
			{
				char tmp=s[i];
				if(tmp=='(' || tmp=='{' || tmp=='[')
					st.push(tmp);
				else if(tmp==')')
				{
					if(st.empty() || st.top()!='(')
						return false;
					else
						st.pop();
				}
				else if(tmp==']')
				{
					if(st.empty() || st.top()!='[')
						return false;
					else
						st.pop();
				}
				else if(tmp=='}')
				{
					if(st.empty() || st.top()!='{')
						return false;
					else
						st.pop();
				}
			}

			if(!st.empty()) return false;
			return true;
		}
};
