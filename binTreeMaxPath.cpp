/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		int maxPathSum(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int maxSum = INT_MIN;

			maxPathSumRec(root, &maxSum);

			return maxSum;
		}

		int maxPathSumRec(TreeNode *root, int *maxSum)
		{
			if(!root->left && !root->right)
			{
				if(*maxSum<root->val)
					*maxSum=root->val;

				return root->val;         
			}

			int leftMax=INT_MIN, rightMax=INT_MIN;

			if(root->left)
				leftMax=maxPathSumRec(root->left, maxSum);
			if(root->right)
				rightMax=maxPathSumRec(root->right, maxSum);

			int tmpMax=root->val;
			if(leftMax>0)
				tmpMax += leftMax;
			if(rightMax>0)
				tmpMax += rightMax;

			if(*maxSum<tmpMax)
				*maxSum=tmpMax;

			tmpMax=max(leftMax, rightMax);
			if(tmpMax>0)
				return (tmpMax+root->val);
			else
				return root->val;
		}
};
