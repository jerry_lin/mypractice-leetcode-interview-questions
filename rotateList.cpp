
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *rotateRight(ListNode *head, int k) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!head || !head->next || k==0) return head;

			int len=0;
			ListNode *p=head;
			while(p) {len++; p=p->next;}

			while(k>=len) k -= len;

			if(k==0) return head;

			int count=0;
			p=head;
			while(count<len-k-1){count++; p=p->next;}

			ListNode *newhead=p->next;
			p->next=NULL;
			p=newhead;
			while(p->next) p=p->next;      
			p->next=head;

			return newhead;
		}
};
