
//Remove Duplicates from Sorted Array II
class Solution {
	public:
		int removeDuplicates(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(n==0) return 0;

			int p=1, q=0;

			bool twice=false;

			while(p<n)
			{
				if(twice && A[q]==A[p]) p++;
				else if(twice && A[q]!=A[p])
				{
					q++;
					A[q]=A[p];
					p++;
					twice=false;
				}
				else if(!twice && A[q]==A[p])
				{
					q++;
					A[q]=A[p];
					p++;
					twice=true;
				}
				else if(!twice && A[q]!=A[p])
				{
					q++;
					A[q]=A[p];
					p++;
				}
			}

			return q+1;
		}
};
