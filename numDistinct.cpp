
//DP: f(i,j)=f(i-1,j)+(S[i-1]==T[j-1]? f(i-1, j-1):0)
class Solution {
	public:
		int numDistinct(string S, string T) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int SLen = S.size(), TLen = T.size();
			if(SLen == 0 || TLen == 0 || SLen < TLen) return 0;

			vector<int> dp(TLen+1, 0);

			dp[0]=1;

			for(int i=1; i<=SLen; i++)
				for(int j=TLen; j>0; j--)
					if(S[i-1]==T[j-1])
						dp[j] += dp[j-1];

			return dp[TLen];
		}
};

class Solution2 {
	public:
		int numDistinct(string S, string T) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int SLen = S.size(), TLen = T.size();
			if(SLen == 0 || TLen == 0 || SLen < TLen) return 0;

			vector<int> dp0(TLen+1, 0);
			vector<vector<int> >dp(2,dp0);

			dp[0][0]=dp[1][0]=1;

			int i=1;
			int prev=0, curr=0;

			while(i<=SLen)
			{
				prev=curr; curr=(curr+1)%2;

				for(int j=1;j<=TLen && j<=i; j++)
				{
					if(S[i-1]==T[j-1])
						dp[curr][j]=dp[prev][j-1]+dp[prev][j];
					else
						dp[curr][j]=dp[prev][j];
				}   
				i++;
			}   
			return dp[curr][TLen];
		}
};

//Recursion
class Solution_Rec {
	public:
		int numDistinct(string S, string T) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int SLen = S.size(), TLen = T.size();
			if(SLen == 0 || TLen == 0) return 0;

			int num=0;

			numDistinct_Rec(S, 0, T, 0, &num);

			return num;
		}

		void numDistinct_Rec(string S, int pos1, string T, int pos2, int *num)
		{
			if(pos2>=T.size()) { (*num)++; return; }

			if(pos1>=S.size()) return;

			if(S[pos1]!=T[pos2])
				numDistinct_Rec(S, pos1+1, T, pos2, num);
			else 
			{
				numDistinct_Rec(S, pos1+1, T, pos2+1, num);
				numDistinct_Rec(S, pos1+1, T, pos2, num);
			}
		}
};
