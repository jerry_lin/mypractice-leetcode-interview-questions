class Solution {
	public:
		bool canJump(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int max=0;
			int idx=0;

			while(idx<=max)
			{    
				if(idx+A[idx]>max)
					max=idx+A[idx];

				if(max>=n-1) return true;

				idx++;
			}

			if(max>=n-1) return true;
			return false;
		}
};
