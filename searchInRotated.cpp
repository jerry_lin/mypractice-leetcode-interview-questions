
class Solution {
	public:
		int search(int A[], int n, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(n==0) return -1;

			int low=0, high=n-1, mid;

			while(low<=high)
			{
				mid = low+(high-low)/2;

				if(A[mid]==target)
					return mid;

				//the bottom half is sorted
				if(A[low]<A[mid])
				{
					if(target>=A[low] && target<A[mid])
						high=mid-1;
					else
						low =mid+1;
				}
				//the upper half is sorted
				//consider difference with A[mid]<A[high]
				else if(A[low]>A[mid]) 
				{
					if(target>A[mid] && target<=A[high])
						low=mid+1;
					else
						high=mid-1;
				}
				else //A[low]==A[mid] not possible here (but when there are duplicates)
					low++;
			}

			return -1;       
		}
};
