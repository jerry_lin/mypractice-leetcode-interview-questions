
class Solution {
	public:
		bool isValidSudoku(vector<vector<char> > &board) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(board.size()==0 || board[0].size()==0) return false;

			int size=board.size();

			vector<bool> each(size+1, false);
			vector<vector<bool> > row(size, each), col(size,each), block(size, each);

			for(int i=0; i<size; i++)
				for(int j=0; j<size; j++)
				{
					if(board[i][j]=='.') continue;

					int val=board[i][j]-'0';

					if(row[i][val]) return false;
					else row[i][val]=true;

					if(col[j][val]) return false;
					else col[j][val]=true;

					int nBlock=(i/3)*3+(j/3);
					if(block[nBlock][val]) return false;
					else block[nBlock][val]=true;
				}
			return true;
		}
};
