
class Solution {
	public:
		vector<vector<int> > generateMatrix(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			if(n==0) return ans;

			vector<int> each(n, -1);
			ans.resize(n, each);

			generateMatrix_rec(ans, 0, 0, 1, n, n);
			return ans;
		}

		void generateMatrix_rec(vector<vector<int> > &ans, int x, int y,
				int start, int n, int size)
		{
			if(size==0) return;
			if(size==1) {ans[x][y]=start; return;}

			int row1=x, row2=n-1-x, col1=y, col2=n-1-y;

			for(int i=col1; i<col2; i++)
				ans[row1][i]=start++;
			for(int i=row1; i<row2; i++)
				ans[i][col2]=start++;
			for(int i=col2; i>col1; i--)
				ans[row2][i]=start++;
			for(int i=row2; i>row1; i--)
				ans[i][col1]=start++;

			generateMatrix_rec(ans, x+1, y+1, start, n, size-2);
		}
};
