
class Solution {
	public:
		vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			vector<int> aAns;

			if(candidates.size()==0) return ans;

			sort(candidates.begin(), candidates.end());

			combinationSum_Rec(candidates, target, 0, aAns, ans);

			return ans;
		}

		void combinationSum_Rec(vector<int> &candidates, int target, int pos, 
				vector<int> aAns, vector<vector<int> > &ans)
		{
			if(pos>=candidates.size() || target < candidates[pos]) return;

			//not include candidates[pos]
			combinationSum_Rec(candidates, target, pos+1, aAns, ans);
			//include candidates[pos]
			aAns.push_back(candidates[pos]);
			if(target==candidates[pos])
				ans.push_back(aAns);
			else
				combinationSum_Rec(candidates, target-candidates[pos], pos, aAns, ans); 
		}
};

class Solution0 {
	public:
		vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			vector<int> aAns;

			if(candidates.size()==0) return ans;

			sort(candidates.begin(), candidates.end());

			for(int i=0; i<candidates.size(); i++)
				combinationSum_Rec(candidates, target, i, aAns, ans);

			return ans;
		}

		void combinationSum_Rec(vector<int> &candidates, int target, int pos, 
				vector<int> aAns, vector<vector<int> > &ans)
		{
			//if(pos>=candidates.size()) return false;
			if(target==candidates[pos])
			{
				aAns.push_back(candidates[pos]);
				ans.push_back(aAns);
			}
			else if(target>candidates[pos])
			{
				for(int i=pos; i<candidates.size(); i++)
				{
					aAns.push_back(candidates[pos]);
					combinationSum_Rec(candidates, target-candidates[pos], i, aAns, ans);
					aAns.pop_back();
				}
			}    
		}
};
