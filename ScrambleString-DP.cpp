#include<iostream>
#include<string>

using namespace std;

class Solution {
	public:
		bool isScramble(string s1, string s2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int n1=s1.length();
			int n2=s2.length();

			if(n1!=n2) return false;

			//rec[i][j][k], if s1.substr(i,k)==s2.substr(j,k)?
			bool***rec = new bool**[n1];
			for(int i=0; i<n1; i++)
			{
				rec[i]=new bool*[n1];
				for(int j=0; j<n1; j++)
				{
					rec[i][j] = new bool[n1+1];
					for(int k=0; k<n1+1; k++)
						rec[i][j][k]=false;
				}
			}

			// 0
			for(int i=0; i<n1; i++)
				for(int j=0; j<n1; j++)
					rec[i][j][1]=(s1.substr(i,1) == s2.substr(j,1));


			// DP
			for(int k=2; k<=n1; k++)
			{
				for(int i=0; i<=n1-k; i++)
					for(int j=0; j<=n1-k; j++)
					{
						for(int m=1; m<k; m++)
						{
							if(rec[i][j][m] && rec[i+m][j+m][k-m]
									||
									rec[i][j+k-m][m] && rec[i+m][j][k-m])
							{
								rec[i][j][k]=true; 
								break;
							}

						}
					}
			}

			/*
			for(int i=0; i<n1; i++)
			{
				cout << i << ':' << endl;
				for(int j=0; j<n1; j++)
				{
					for(int k=1; k<n1+1; k++)
					{
						cout << rec[i][j][k] << ' ';
					}
					cout << endl;
				}
			}
			*/
			bool result = rec[0][0][n1];

			return result;

		}

};

string s1="aa";
string s2="aa";

int main()
{
	Solution sol;
	
	if(sol.isScramble(s1, s2))
		cout << true << endl;
	else 
		cout << false << endl;

	return 0;
}
