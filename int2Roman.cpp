
class Solution {
	public:
		string intToRoman(int num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function    
			string symbol="IVXLCDM";// 1,5,10,50,100,500,1000

			int len=symbol.size();

			string res;

			for(int base=0; num>0; base+=2, num=num/10)
			{
				int digit=num%10;

				switch(digit)
				{
					case 1: case 2: case 3:
						res = string(digit, symbol[base])+res;
						break;
					case 4:
						res = symbol[base+1]+res;
						res = symbol[base]+res;
						break;
					case 5:
						res = symbol[base+1]+res;
						break;
					case 6: case 7: case 8:
						res = string(digit-5, symbol[base])+res;
						res = symbol[base+1]+res;
						break;
					case 9:
						res = symbol[base+2]+res;
						res = symbol[base]+res;
						break;
					default:
						break;
				}

			}
			return res;
		}
};
