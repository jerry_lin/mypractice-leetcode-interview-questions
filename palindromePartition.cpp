
class Solution {
	public:
		vector<vector<string>> partition(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<string> > ans;
			vector<string> aAns;

			if(s.size()==0) return ans;

			partition_rec(s, 0, ans, aAns);

			return ans;
		}

		void partition_rec(string &s, int pos, vector<vector<string> > &ans,
				vector<string> aAns)
		{
			if(pos>=s.size())
			{
				ans.push_back(aAns);
				return;
			}

			for(int i=pos; i<s.size(); i++)
			{
				string str = s.substr(pos, i-pos+1);
				if(isPalindrome(str))
				{
					aAns.push_back(str);
					partition_rec(s, i+1, ans, aAns);
					aAns.pop_back();
				}            
			}
		}

		bool isPalindrome(string ss)
		{
			int low=0, high=ss.size()-1;
			while(low<=high)
			{
				if(ss[low]!=ss[high]) return false;

				low++; high--;
			}
			return true;
		}
};
