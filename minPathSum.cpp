class Solution {
	public:
		int minPathSum(vector<vector<int> > &grid) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(grid.size()==0 || grid[0].size()) return INT_MAX;

			int height = grid.size();
			int width  = grid[0].size();

			vector<int> res(width+1, INT_MAX);
			res[1]=0;

			for(int i=0; i<height; i++)
				for(int j=0; j<width; j++)
					res[j+1]=min(res[j], res[j+1])+grid[i][j];

			return res[width];
		}
};
