#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Solution {
	public:
		vector<string> letterCombinations(string digits) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<string> ans;
			string s="";
			if(digits.length() == 0)
				ans.push_back(s);
			else
				letterRec(digits, 0, ans, s);
			return ans;
		}

		void letterRec(string digits, int pos, vector<string>& ans, string s)
		{
			const static string key_maps[10]={
				"0","1","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

			if(pos==digits.length())
			{
				ans.push_back(s);
				return;
			}
			int num=digits[pos]-'0';
			for(int j=0; j<key_maps[num].length();j++)
				letterRec(digits, pos+1, ans, s+key_maps[num][j]);
		}
};


int main()
{
	Solution sol;
	string str="23";

	sol.letterCombinations(str);

	return 0;
}
