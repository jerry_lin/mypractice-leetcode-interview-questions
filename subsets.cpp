class Solution {
	public:
		vector<vector<int> > subsets(vector<int> &S) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			sort(S.begin(), S.end()); //not necessary, only for passing test cases

			int sz=S.size();

			int max = 1<<sz;

			for(int i=0; i<max; i++)
			{
				vector<int> aAns;
				int index=0;
				int k=i;

				while (k > 0)
				{
					if(k&1)
						aAns.push_back(S[index]);
					k >>= 1;
					index++;
				}  
				ans.push_back(aAns);
			}

			return ans;
		}
};
