
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		int sumNumbers(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!root) return 0;

			int ans=0;

			sumNum_Rec(root, 0, &ans);

			return ans;
		}

		void sumNum_Rec(TreeNode *root, int val, int *ans)
		{
			if(!root->left && !root->right)
			{
				*ans += val*10+root->val;
				return;
			}

			if(root->left)
				sumNum_Rec(root->left, val*10+root->val, ans);
			if(root->right)
				sumNum_Rec(root->right, val*10+root->val, ans);
		}
};
