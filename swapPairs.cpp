/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *swapPairs(ListNode *head) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(!head || !head->next) return head;

			ListNode *n1, *prev;

			ListNode *tmp_head=new ListNode(-1);
			tmp_head->next = head;
			prev=tmp_head;

			n1=head;

			while(n1 && n1->next)
			{
				ListNode *n2 = n1->next;
				n1->next=n2->next;
				prev->next=n2;
				n2->next=n1;

				prev=n1;
				n1=n1->next;
			}

			ListNode *ans = tmp_head->next;
			delete tmp_head;
			return ans;
		}
};
