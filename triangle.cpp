
class Solution {
	public:
		int minimumTotal(vector<vector<int> > &triangle) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int h=triangle.size();

			if(h==0) return 0;

			vector<int> dp(h, 0);

			for(int i=0; i<h; i++)
				for(int j=i; j>=0; j--)
				{
					if(j==0) dp[j]+= triangle[i][j];
					else if(j==i) dp[j] = dp[j-1]+triangle[i][j];
					else dp[j]=min(dp[j], dp[j-1])+triangle[i][j];
				}

			int minV=dp[0];
			for(int i=1; i<h; i++)
				if(minV>dp[i]) minV=dp[i];

			return minV;
		}
};
