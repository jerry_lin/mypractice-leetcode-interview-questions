class Solution {
	public:
		vector<int> grayCode(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<int> ans;
			int num= 1 << n;
			for(int i=0; i<num; i++)
				ans.push_back(i^(i>>1));
			return ans;
		}
};
