class Solution {
	public:
		vector<vector<int> > permute(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			vector<vector<int> > ans;

			if(num.size<=0) return ans;

			vector<int> s;
			vector<bool> used(len, false);

			permuteRec(num, s, used, ans);

			return ans;
		}

		void permuteRec(vector<int> &num, vector<int> &s, vector<bool> &used, 
				vector<vector<int> > &ans)
		{
			if(s.size()==num.size())
			{
				ans.push_back(s);
				return;
			}

			for(int i=0; i<used.size(); i++)
			{
				if(!used[i])
				{
					s.push_back(num[i]);
					used[i]=true;
					permuteRec(num, s, used, ans);
					used[i]=false;
					s.pop_back();                
				}

			}   
		}
};
