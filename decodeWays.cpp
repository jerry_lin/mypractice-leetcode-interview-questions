


class Solution {
	public:
		int numDecodings(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int len=s.size();
			if(len==0) return 0;

			int f[3]={0};

			if(s[0]=='0') return 0;
			else {f[0]=1;f[1]=1;}

			for(int i=1; i<len; i++)
			{
				int idx1=(i+2)%3;
				int idx2=i%3;
				int idx3=(i+1)%3;

				if(s[i]!='0') f[idx3]=f[idx2];
				else f[idx3]=0;

				int tmp=(s[i-1]-'0')*10+s[i]-'0';

				if(tmp>=10 && tmp<=26) f[idx3]+=f[idx1];                   
			}
			return f[len%3];
		}
};


class Solution_rec {
	public:
		int numDecodings(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len=s.size();
			if(len==0) return 0;

			return numDecodings_rec(s, 0);
		}

		int numDecodings_rec(string s, int pos)
		{
			if(pos==s.size()) return 1;
			else if(s[pos]=='0') return 0;
			else if(pos+1<s.size() )
			{
				int tmp=(s[pos]-'0')*10+(s[pos+1]-'0');
				if(tmp<=26)
					return numDecodings_rec(s, pos+1)
						+numDecodings_rec(s,pos+2);
				else
					return numDecodings_rec(s,pos+1);
			}
			else
				return numDecodings_rec(s, pos+1);
		}
};
