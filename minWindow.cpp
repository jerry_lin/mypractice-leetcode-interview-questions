#include<iostream>
#include<string>

using namespace std;



class Solution {
	public:
		string minWindow(string S, string T) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int needed[256]={0};
			int found[256]={0};

			int nS=S.length(), nT=T.length();
			string ans="";

			if(nS<nT) return ans;

			for(int i=0; i<nT; i++)
				needed[T[i]]++;

			int start=0, end=0;
			int count=0;

			for(end=0; end<nS; end++)
			{
				char charEnd=S[end];
				found[charEnd]++;
				if(found[charEnd]<=needed[charEnd])
					count++;

				if(count < nT) continue;

				char charStart=S[start];
				while(found[charStart]>needed[charStart] && start<=end)
				{
					found[charStart]--;
					start++;
					charStart=S[start];
				}
				if(ans.length()==0 || end-start+1 < ans.length())
					ans=S.substr(start, end-start+1);
			}
			return ans;
		}
};

string str1="bdab";
string str2="ab";

int main()
{
	Solution sol;
	string ans = sol.minWindow(str1, str2);
	cout << ans << endl;

	return 0;
}
