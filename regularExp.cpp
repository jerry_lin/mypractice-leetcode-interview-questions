
//O(P) space, O(PS) time
class Solution {
	public:
		bool isMatch(const char *s, const char *p) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(s==NULL || p==NULL) return false;

			int sLen=strlen(s), pLen=strlen(p);

			vector<bool> canMatch0(pLen+1, false);
			vector<vector<bool> > canMatch(2, canMatch0);

			canMatch[0][0]=true;
			for(int j=0; j<pLen; j++)
				if(p[j]=='*') canMatch[0][j+1]=canMatch[0][j-1];

			int curr=0, prev=0;

			for(int i=1; i<=sLen; i++)
			{
				prev=curr;
				curr=(curr+1)%2;

				canMatch[curr][0]=false;

				for(int j=1; j<=pLen; j++)
				{
					if(p[j-1]=='*') // be careful here
						canMatch[curr][j]=canMatch[curr][j-2] 
							|| canMatch[curr][j-1]
							|| canMatch[prev][j] && (s[i-1]==p[j-2]||p[j-2]=='.');
					else if(p[j-1]=='.' || p[j-1]==s[i-1])
						canMatch[curr][j]=canMatch[prev][j-1];
					else
						canMatch[curr][j]=false;
				}
			}
			return canMatch[curr][pLen];
		}
};

class Solution_Rec {
	public:
		bool isMatch(const char *s, const char *p) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!s || !p) return false;

			if(*p=='\0') return *s=='\0';

			if(*(p+1)!='*')
			{
				if(*p==*s || *p=='.' && *s!='\0') 
					return isMatch(s+1, p+1);
				else
					return false;
			}

			while(*p==*s || *p=='.' && *s!='\0')
			{
				if(isMatch(s, p+2) ) return true;
				s++;
			}
			return isMatch(s,p+2);      
		}
};
