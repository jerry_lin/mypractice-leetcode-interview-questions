/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		void flatten(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function                
			flattern_Rec(root);
		}

		TreeNode *flattern_Rec(TreeNode *root)
		{
			if(!root || !root->left && !root->right)
				return root;

			TreeNode *lefttail = flattern_Rec(root->left);
			TreeNode *righttail = flattern_Rec(root->right);

			if(lefttail) 
			{
				lefttail->right = root->right;
				root->right = root->left;
				root->left=NULL;
			}

			if(righttail) 
				return righttail;
			else 
				return lefttail;        
		}

};

/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution2 {
	public:
		void flatten(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function        
			if(root)        
				flattern_Rec(root);
		}

		TreeNode *flattern_Rec(TreeNode *root)
		{
			if(!root->left && !root->right)
				return root;
			else if(!root->left && root->right)
				return flattern_Rec(root->right);
			else if(root->left && !root->right)
			{
				root->right=root->left;
				root->left=NULL;
				return flattern_Rec(root->right);
			}
			else
			{
				TreeNode *tmp=root->right;
				TreeNode *end=flattern_Rec(root->left);
				root->right=root->left;
				root->left=NULL;
				end->right=tmp;
				return flattern_Rec(root->right);
			}
		}

};
