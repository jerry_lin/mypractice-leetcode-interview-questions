
class Solution {
	public:
		int climbStairs(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int res[3]={1,1,2};

			for(int i=3; i<=n; i++)
				res[i%3] = res[(i-1)%3]+res[(i-2)%3];

			return res[n%3];
		}

};

class Solution_Recursive {
	public:
		int climbStairs(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			return climbStairsRec(n);
		}


		int climbStairsRec(int n)
		{
			if(n==0) return 1;

			if(n==1) return 1;

			return climbStairsRec(n-1)+climbStairsRec(n-2);
		}
};
