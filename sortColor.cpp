
class Solution {
	public:
		void sortColors(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(n<=1) return;
			int left=0, right=n-1, p=0;
			while(p<=right)
			{
				if(A[p]==0)
				{
					swap(&A[left], &A[p]);
					left++;
					p++;
				}
				else if(A[p]==2)
				{
					swap(&A[right], &A[p]);
					right--;
					//not advance p here, as A[p] could be any value
				}
				else
				{
					p++;
				}
			}
		}

		void swap(int *a, int *b)
		{
			int tmp;
			tmp=*b;
			*b=*a;
			*a=tmp;
		}
};
