
class Solution {
	public:
		void solveSudoku(vector<vector<char> > &board) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int size=board.size();

			vector<bool> each(size+1, false);
			vector<vector<bool> > row(size, each), col(size, each), block(size, each);

			init(board, row, col, block);

			solve(board, row, col, block, 0, 0);
		}

		void init(vector<vector<char> > &board, vector<vector<bool> > &row,
				vector<vector<bool> > &col, vector<vector<bool> > &block)
		{
			for(int i=0; i<board.size(); i++)
				for(int j=0; j<board.size(); j++)
				{
					if(board[i][j]=='.') continue;

					int val=board[i][j]-'0';

					row[i][val]=true;
					col[j][val]=true;
					block[(i/3)*3+(j/3)][val]=true;
				}
		}

		bool solve(vector<vector<char> > &board, vector<vector<bool> > &row,
				vector<vector<bool> > &col, vector<vector<bool> > &block,
				int x, int y)
		{
			if(x>=board.size()) return true;

			int x_next, y_next;
			if(y>=board.size()-1){x_next=x+1; y_next=0;}
			else {x_next=x; y_next=y+1;}

			if(board[x][y]!='.') 
				return solve(board,row, col, block, x_next, y_next);
			else
			{
				int nBlock=(x/3)*3+y/3;
				for(int val=1; val<=9; val++)
				{
					if(!row[x][val] && !col[y][val] && !block[nBlock][val])
					{
						row[x][val]=col[y][val]=block[nBlock][val]=true;
						board[x][y]='0'+val;

						if(solve(board,row,col,block,x_next,y_next) )                
							return true;

						board[x][y]='.';                    
						row[x][val]=col[y][val]=block[nBlock][val]=false;
					}
				}
				return false;
			}
		}
};
