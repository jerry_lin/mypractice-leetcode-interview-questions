
/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
	public:
		void connect(TreeLinkNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!root) return;

			queue<TreeLinkNode *> myQueue;

			myQueue.push(root);

			while(!myQueue.empty())
			{
				queue<TreeLinkNode *> tmpQueue;
				TreeLinkNode *prev=NULL, *curr=NULL;

				while(!myQueue.empty())
				{
					prev=curr;
					curr=myQueue.front();
					myQueue.pop();
					if(curr->left) tmpQueue.push(curr->left);
					if(curr->right) tmpQueue.push(curr->right);

					if(prev) prev->next=curr;                
				}
				myQueue=tmpQueue;
			}
		}


};
