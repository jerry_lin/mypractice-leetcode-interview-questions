
class Solution {
	public:
		bool search(int A[], int n, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(n==0) return false;

			int low=0, high=n-1, mid;

			while(low<=high)
			{
				mid = low+(high-low)/2;

				if(A[mid]==target)
					return true;

				//the bottom half is sorted
				if(A[low]<A[mid])
				{
					if(target>=A[low] && target<A[mid])
						high=mid-1;
					else
						low =mid+1;
				}
				//the upper half is sorted
				//consider difference with A[mid]<A[high], which does not work!!!!!
				//else if(A[mid]<A[high]) // counter-example ( [3,1,1], 3 )
				else if(A[low]>A[mid])
				{
					if(target>A[mid] && target<=A[high])
						low=mid+1;
					else
						high=mid-1;
				}
				else //when no clue in which part, because of duplicates
					low++;
			}

			return false;
		}
};
