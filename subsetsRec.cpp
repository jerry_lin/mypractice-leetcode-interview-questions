#include<iostream>
#include<algorithm>
#include<vector>
#include<string>

using namespace std;

class Solution {
	public:
		vector<vector<int> > subsets(vector<int> &S) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector<int> > ans;
			vector<int> aAns;

			sort(S.begin(), S.end()); //for passing test cases

			subsetsRec(S, ans, aAns, 0);

			return ans;
		}

		void subsetsRec(vector<int> &S, vector<vector<int> > &ans, 
				vector<int> aAns, int pos)
		{
			if(pos==S.size())
			{
				ans.push_back(aAns);
				return;
			}

			subsetsRec(S, ans, aAns, pos+1);
			aAns.push_back(S[pos]);
			subsetsRec(S, ans, aAns, pos+1);
			//aAns.pop_back(); // use this if using &aAns
		}
};

int main()
{
	Solution sol;

	vector<int> S;
	for(int i=0; i<4; i++)
		S.push_back(i);

	sol.subsets(S);

	return 0;
}
