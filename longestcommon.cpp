class Solution {
	public:
		string longestCommonPrefix(vector<string> &strs) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int n=strs.size();        
			if(n<=0) return "";

			for(int i=0; i<strs[0].length(); i++)
				for(int j=1;j<n;j++)
				{
					if(strs[j].length()<i || strs[j][i] != strs[0][i])
						return strs[0].substr(0,i);
				}
			return strs[0];
		}
};
