#include<iostream>
#include<vector>
#include<map>
#include<string>

using namespace std;

class Solution {
	public:
		vector<int> findSubstring(string S, vector<string> &L) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			map<string, int> dict;
			vector<int> ans;
			int nS=S.size(), nL=L.size();

			if(nS==0 || nL == 0) return ans;

			int len=L[0].size();
			vector<int> needed(nL, 0);

			for(int i=0; i<nL; i++)
			{
				if(!dict.count(L[i])) dict[L[i]]=i;
				needed[dict[L[i]]]++;
			}

			vector<int> index(nS, -1);
			for(int i=0; i<nS-len+1; i++)
			{
				string tmp=S.substr(i, len);
				if(dict.count(tmp))
					index[i]=dict[tmp];
			}

			//find the windows containning all words
			for(int offset=0; offset<len; offset++)
			{
				vector<int> found(nL, 0);
				int begin=offset;
				int count=0;

				for(int j=offset; j<nS-len+1; j=j+len)
				{                
					int tmpIndex=index[j];
					if(tmpIndex==-1)
					{
						begin=j+len;
						found.clear();
						found.resize(nL, 0);
						count=0;
					}
					else //put into the window if valid
					{
						found[tmpIndex]++;
						if(needed[tmpIndex] 
								&& found[tmpIndex]<=needed[tmpIndex])
							count++;
						else if(needed[tmpIndex] 
								&& found[tmpIndex]>needed[tmpIndex])   
						{
							while(found[tmpIndex]>needed[tmpIndex])
							{
								found[index[begin]]--;
								begin=begin+len;
							}
						}
						
						//all included
						if(count==nL)
						{
							ans.push_back(begin);
							found[index[begin]]--;
							count--;
							begin=begin+len;
						}

					}
				}
			}
			return ans;
		}
};


int main()
{
	string S="barfoothefoobarman";
	vector<string> L;
	L.push_back("bar");
	L.push_back("foo");

	Solution sol;
	
	vector<int> ans=sol.findSubstring(S, L);

	return 0;
}
