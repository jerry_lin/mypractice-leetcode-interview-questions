/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *reverseBetween(ListNode *head, int m, int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			ListNode *reverse_head=NULL, *reverse_end=NULL, *curr;
			int count=0;

			ListNode *tmp_head = new ListNode(-1);
			tmp_head->next=head;
			curr=tmp_head;

			if(m==n) return head;

			while(curr)
			{
				if(count<m)
				{
					reverse_head=curr;
					curr=curr->next;
					reverse_end=curr;
					count++;
				}
				else if(count<=n)
				{
					ListNode *next = curr->next;
					curr->next=reverse_head->next;
					reverse_head->next=curr;
					reverse_end->next=next;

					curr=next;
					count++;
				}
				else
					break;
			}

			head=tmp_head->next;
			delete tmp_head;
			return head;
		}
};
