
class Solution {
	public:
		vector<int> searchRange(int A[], int n, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<int> ans(2, -1);
			if(n==0) return ans;

			int low=0, high=n-1, mid;

			while(low<high)
			{
				mid=low+(high-low)/2;

				if(A[mid]==target)
					high=mid;
				else if(A[mid]>target)
					high=mid-1;
				else
					low=mid+1;
			}
			if(low==high && A[low]==target)
				ans[0]=low;
			else
				return ans;

			low=0;
			high=n-1;
			while(low<high)
			{
				mid=high-(high-low)/2;

				if(A[mid]==target)
					low=mid;
				else if(A[mid]>target)
					high=mid-1;
				else
					low=mid+1;
			}

			//should exist here
			ans[1]=low;

			return ans;     
		}
};
