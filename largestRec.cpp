
class Solution {
	public:
		int largestRectangleArea(vector<int> &height) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len=height.size();
			if(len==0) return 0;

			stack<int> st;

			int rightmost, currMax=height[0];

			for(int i=0; i<len; i++)
			{
				if(st.empty() || height[i]>=height[st.top()])
				{
					st.push(i);
					continue;
				}

				rightmost=st.top();
				while(!st.empty() && height[st.top()]>height[i])
				{
					int h=height[st.top()];
					st.pop();
					int width = st.empty()? rightmost+1: rightmost-st.top();

					int tmpArea=h*width;
					currMax=max(currMax, tmpArea);
				}
				st.push(i);
			}

			if(!st.empty()) rightmost=st.top();

			while(!st.empty())
			{
				int h=height[st.top()];
				st.pop();
				int width = st.empty()? rightmost+1: rightmost-st.top();

				int tmpArea=h*width;
				currMax=max(currMax, tmpArea);
			}
			return currMax;
		}
};


/*
class Solution0 {
	public:
		int largestRectangleArea(vector<int> &height) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int sz=height.size();

			if(sz<=0) return 0;

			stack<int> st;
			int maxArea=0;

			int i=0;
			while(i<sz)
			{
				if(st.empty())
					st.push(i);
				else if(height[st.top()]<height[i])
					st.push(i);
				else
				{
					int rightmost=st.top();
					while(!st.empty() && height[st.top()]>=height[i])
					{
						int tmp=st.top();
						st.pop();
						if(st.empty())
							maxArea=max(maxArea, height[tmp]*(rightmost+1));
						else
							maxArea=max(maxArea, height[tmp]*(rightmost-st.top()));
					}

*/
