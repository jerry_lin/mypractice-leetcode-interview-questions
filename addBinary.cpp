
class Solution {
	public:
		string addBinary(string a, string b) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int a_sz=a.size();
			int b_sz=b.size();

			if(a_sz==0) return b;
			if(b_sz==0) return a;

			int sz=max(a_sz, b_sz);

			if(a_sz>b_sz)
			{
				string prefix(a_sz-b_sz, '0');
				b=prefix+b;
			}
			else if(b_sz>a_sz)
			{
				string prefix(b_sz-a_sz, '0');
				a=prefix+a;
			}

			string ans="";

			int idx=sz-1;
			int carry=0;

			while(idx>=0)            
			{
				int sum=(a[idx]-'0')+(b[idx]-'0')+carry;

				if(sum>1) 
				{
					carry=1;
					sum=sum-2;
				}
				else
					carry=0;

				char sum_char=sum+'0';
				ans = sum_char+ans;

				idx--;
			}


			if(carry)
				ans = '1'+ans;

			return ans;
		}
};



class Solution2 {
	public:
		string addBinary(string a, string b) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int a_sz=a.size();
			int b_sz=b.size();

			if(a_sz==0) return b;
			if(b_sz==0) return a;

			char *a_str = new char[a_sz+1];
			char *b_str = new char[b_sz+1];

			int res_sz=max(a_sz, b_sz);
			int *res = new int[res_sz+1];

			strcpy(a_str, a.c_str());
			strcpy(b_str, b.c_str());


			int a_idx=a_sz-1, b_idx=b_sz-1, res_idx=res_sz-1;
			int carry=0;

			while(a_idx>=0 && b_idx>=0)            
			{
				int tmp=(a_str[a_idx]-'0')+(b_str[b_idx]-'0')+carry;

				if(tmp>1) 
				{
					carry=1;
					tmp=tmp-2;
				}
				else
					carry=0;

				res[res_idx]=tmp;

				a_idx--;
				b_idx--;
				res_idx--;
			}

			while(a_idx>=0)            
			{
				int tmp=(a_str[a_idx]-'0')+carry;

				if(tmp>1) 
				{
					carry=1;
					tmp=tmp-2;
				}
				else
					carry=0;

				res[res_idx]=tmp;

				a_idx--;
				res_idx--;
			}

			while(b_idx>=0)            
			{
				int tmp=(b_str[b_idx]-'0')+carry;

				if(tmp>1) 
				{
					carry=1;
					tmp=tmp-2;
				}
				else
					carry=0;

				res[res_idx]=tmp;

				b_idx--;
				res_idx--;
			}


			string ans="";

			if(carry)
				ans += ('0'+1);

			for(int i=0; i<res_sz; i++)
				ans += ('0'+res[i]);


			return ans;
		}
};
