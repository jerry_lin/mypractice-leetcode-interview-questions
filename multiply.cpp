class Solution {
	public:
		string multiply(string num1, string num2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int n1=num1.size(), n2=num2.size();

			vector<int> ans(n1+n2, 0);

			for(int i=0; i<n1; i++)
			{
				int carry=0;
				for(int j=0; j<n2; j++)
				{
					int mul=(num1[n1-i-1]-'0')*(num2[n2-j-1]-'0');
					mul = mul+ans[i+j]+carry;
					carry=mul/10;
					mul=mul%10;
					ans[i+j]=mul;
				}
				if(carry)
					ans[i+n2]=carry;
			}

			string ans1="";
			int index=ans.size()-1;

