class Solution {
	public:
		string longestPalindrome(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int n = s.length();
			int nlongest=0, start=0, end=0;
			for(int i=0; i<n; i++)
			{
				int p1=i, p2=i;
				while(1)
				{
					if(p1<0 || p2>=n || s[p1]!=s[p2])
					{
						if(p2-p1-1>nlongest)
						{
							nlongest=p2-p1-1;
							start=p1+1;
							end=p2-1;
						}
						break;
					}
					else
					{
						p1--;
						p2++;
					}
				} //p1 p2

				p1=i; p2=i+1;
				while(1)
				{
					if(p1<0 || p2>=n || s[p1]!=s[p2])
					{
						if(p2-p1-1>nlongest)
						{
							nlongest=p2-p1-1;
							start=p1+1;
							end=p2-1;
						}
						break;
					}
					else
					{
						p1--;
						p2++;
					}
				} //p1 p2
			} //for 
			return s.substr(start, end-start+1);
		}

};
