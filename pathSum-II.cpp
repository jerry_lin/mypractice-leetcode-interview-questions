/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		vector<vector<int> > pathSum(TreeNode *root, int sum) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			vector<int> aAns;
			vector<vector<int> > ans;

			if(!root) return ans;

			sumRec(root, 0, sum, aAns, ans);

			return ans;
		}

		void sumRec(TreeNode *root, int sum_so_far, int sum, 
				vector<int> aAns, vector<vector<int> > &ans)
		{
			aAns.push_back(root->val);
			sum_so_far += root->val;

			if(!root->left && !root->right)
			{
				if(sum_so_far == sum)
					ans.push_back(aAns);
			}
			else
			{
				if(root->left)
					sumRec(root->left, sum_so_far, sum, 
							aAns, ans);
				if(root->right)
					sumRec(root->right, sum_so_far, sum, 
							aAns, ans);
			}       

		}
};
