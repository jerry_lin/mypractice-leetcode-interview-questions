
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
public boolean hasPathSum(TreeNode root, int sum) {
	// Start typing your Java solution below
	// DO NOT write main() function
	if(root==null)
		return false;
	if(root.left==null && root.right==null && root.val==sum)
		return true;
	else
		return hasPathSum(root.left, sum-root.val)||hasPathSum(root.right,sum-root.val);
};



class Solution2 {
	public:
		bool hasPathSum(TreeNode *root, int sum) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!root) return false;

			return sumRec(root, 0, sum);
		}

		bool sumRec(TreeNode *root, int sum_so_far, int sum)
		{
			if(!root->left && !root->right)
				return (sum_so_far+root->val == sum);

			if(root->left)
				if(sumRec(root->left, sum_so_far+root->val, sum))
					return true;
			if(root->right)
				if(sumRec(root->right, sum_so_far+root->val, sum))
					return true;

			return false;
		}
};


