/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		TreeNode *sortedListToBST(ListNode *head) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(!head) return NULL;

			int len = 0;
			ListNode *p=head;
			while(p) {len++; p=p->next;}

			return sortedListToBST_Rec(head, 0, len-1);
		}

		TreeNode *sortedListToBST_Rec(ListNode *&list, int low, int high)
		{
			if(low>high) return NULL;

			int mid = low+(high-low)/2;

			TreeNode *leftChild = sortedListToBST_Rec(list, low, mid-1);
			
			TreeNode *root=new TreeNode(list->val);//list is the root

			list = list->next;//move to the head of right subtree
			TreeNode *rightChild = sortedListToBST_Rec(list, mid+1, high);

			root->left=leftChild;
			root->right=rightChild;
			return root;
		}   
};
