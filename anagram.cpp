#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<cstring>
#include<stdlib.h>

using namespace std;

int cmp(const void *a, const void *b)
{
	return *(char *)a - *(char *)b;
}

string sortString(string str)
{
	char *cstr = new char[str.size()+1];

	strcpy(cstr, str.c_str());
	qsort(cstr, str.size(), sizeof(char), cmp);
	return string(cstr);
}

bool isAnagram(string s1, string s2)
{
	int sz1=s1.size(), sz2=s2.size();
	if(sz1!=sz2) 
		return false;

	if(sortString(s1) == sortString(s2))
		return true;
	else
		return false;
}

bool isAnagram2(string s1, string s2)
{
	int sz1=s1.size(), sz2=s2.size();
	if(sz1!=sz2)
		return false;

	vector<int> nLetters(256, 0);

	for(int i=0; i<sz1; i++)
		nLetters[s1[i]]++;

	for(int i=0; i<sz1; i++)
	{
		nLetters[s2[i]]--;

		if(nLetters[s2[i]] < 0) 
			return false;
	}
	return true;
}

int main()
{
	string s1("abcd");
	string s2("cbaf");

	if(isAnagram(s1,s2))
		cout << "T" << endl;
	else
		cout << "F" << endl;

	if(isAnagram2(s1,s2))
		cout << "T" << endl;
	else
		cout << "F" << endl;


	return 0;
}
