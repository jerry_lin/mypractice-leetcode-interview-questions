
class Solution {
	public:
		vector<string> restoreIpAddresses(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<string> ans;
			string aAns;
			if(s.size()==0) return ans;

			restoreIpAddresses_Rec(s, aAns, ans, 0, 0);

			return ans;
		}

		void restoreIpAddresses_Rec(string s, string aAns, vector<string> &ans, 
				int pos, int sec)
		{
			if(sec<4 && pos>=s.size() || sec>=4 && pos<s.size()) return;
			if(pos>=s.size() && sec>=4)
			{
				ans.push_back(aAns);
				return;          
			}     

			int pos2=pos;
			int tmp=s[pos2]-'0';
			while(tmp<=255)
			{
				if(sec==0)
					restoreIpAddresses_Rec(s, aAns+s.substr(pos, pos2-pos+1), ans, pos2+1, sec+1);
				else
					restoreIpAddresses_Rec(s, aAns+"."+s.substr(pos, pos2-pos+1), ans, pos2+1, sec+1);

				if(tmp==0) break;//take are if 0 is at the beginning
				if(++pos2>=s.size()) break;

				tmp=tmp*10+(s[pos2]-'0');
			}
		}

};
