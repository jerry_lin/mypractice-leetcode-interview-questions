
class Solution {
	public:
		bool isPalindrome(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(s.size()==0) return true;

			int low=0, high=s.size()-1;
			while(low<=high)
			{
				int c_low=isAlphanumeric(s[low]);
				int c_high=isAlphanumeric(s[high]);

				if(c_low && c_high)
				{
					if(c_low!=c_high) return false;
					low++; high--;
				}
				else if(!c_low) low++;
				else if(!c_high) high--;
			}
			return true;
		}    

		int isAlphanumeric(char c)
		{
			if(c>='a' && c<='z' || c>='A' && c<='Z')
				return tolower(c);
			else if(c>='1' && c<='9' || c=='0')
				return c;
			else 
				return 0;
		}   
};
