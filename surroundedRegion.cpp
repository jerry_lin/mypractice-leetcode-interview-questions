
class Solution {
	public:
		class Cell
		{
			public:
				int x;
				int y;

			public:
				Cell(int x, int y):x(x), y(y){}
		};

		void solve(vector<vector<char>> &board) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(board.size()==0 || board[0].size()==0) return;

			int row = board.size(), col = board[0].size();

			vector<bool> visited0(col, false);
			vector<vector<bool> > visited(row, visited0);

			queue<Cell> Q1;

			for(int i=0; i<row; i++)
				for(int j=0; j<col;j++)
				{
					if(board[i][j]=='X' || visited[i][j]) continue;

					bool canFlip=true;
					Q1.push(Cell(i,j));
					visited[i][j]=true;
					queue<Cell> Q2;
					if(i==0 || i==row-1 || j==0 || j==col-1)
						canFlip=false;

					while(!Q1.empty())
					{
						Cell curr = Q1.front();
						Q1.pop();
						Q2.push(curr);

						visit(curr.x-1, curr.y, row, col, board, visited, Q1, canFlip);
						visit(curr.x+1, curr.y, row, col, board, visited, Q1, canFlip);
						visit(curr.x, curr.y-1, row, col, board, visited, Q1, canFlip);
						visit(curr.x, curr.y+1, row, col, board, visited, Q1, canFlip);
					}

					if(canFlip)
					{
						while(!Q2.empty())
						{
							Cell curr=Q2.front();
							Q2.pop();
							board[curr.x][curr.y]='X';
						}
					}
				}
		}

		void visit(int i, int j, int row, int col, vector<vector<char>> &board,
				vector<vector<bool> >&visited, queue<Cell> &Q1, bool &canFlip)
		{
			if(i>=row || i<0 || j>=col || j<0) return;

			if(board[i][j]=='X' || visited[i][j]) return;

			if(i==row-1 || i==0 || j==col-1 || j==0) canFlip=false;

			visited[i][j]=true;
			Q1.push(Cell(i,j));
		}
};
