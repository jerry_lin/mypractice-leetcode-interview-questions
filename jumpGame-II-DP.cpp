class Solution {
	public:
		int jump(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int *minSteps = new int[n];

			for(int i = 0; i<n; i++)
				minSteps[i]=INT_MAX-1;

			minSteps[n-1]=0;

			for(int i=n-2; i>=0; i--)
			{
				for(int j=1; j<=A[i] && i+j<n; j++)
					minSteps[i]=min(minSteps[i], minSteps[i+j]+1);            
			}

			int ans=minSteps[0];
			delete minSteps;

			return ans;
		}
};
