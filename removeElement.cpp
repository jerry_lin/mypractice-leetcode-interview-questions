
class Solution {
	public:
		int removeElement(int A[], int n, int elem) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int i=0, j=n-1;

			while(i<=j)
			{
				if(A[i]==elem)
				{
					A[i]=A[j];
					j--;
				}
				else
					i++;
			}
			return j+1;
		}
};
