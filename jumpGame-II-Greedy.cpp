class Solution {
	public:
		int jump(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int minSteps=0;

			int max=0, preMax=0, idx=0;
			while(idx<=max)
			{
				if(idx==n-1) return minSteps;

				if(idx>preMax)
				{
					preMax=max;
					minSteps++;
				}

				if(max<idx+A[idx])
					max=idx+A[idx];

				if(max>=n-1) return (minSteps+1);

				idx++;
			}

			return INT_MAX;//cannot reach

		}
};
