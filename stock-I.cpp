class Solution {
	public:
		int maxProfit(vector<int> &prices) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(prices.size()==0) return 0;

			int minPrice=prices[0], maxPrice=prices[0];
			int ans=0;

			for(int i=0; i<prices.size(); i++)
			{
				int tmp=prices[i];

				if(tmp<minPrice)
				{
					ans=max(ans, maxPrice-minPrice);
					minPrice=tmp;
					maxPrice=tmp;
				}
				if(tmp>maxPrice)
					maxPrice=tmp;
			}

			return max(ans, maxPrice-minPrice);
		}
};
