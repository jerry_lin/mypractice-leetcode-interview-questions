/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		vector<vector<int> > levelOrderBottom(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<vector <int> > ans;
			if(!root) return ans;

			vector<int> level;
			queue<TreeNode *> nodeQueue;

			nodeQueue.push(root);
			int n_prev=1;
			int n_next=0;

			while(!nodeQueue.empty())
			{
				TreeNode *front=nodeQueue.front();
				level.push_back(front->val);
				if(front->left)
				{
					nodeQueue.push(front->left);
					n_next++;
				}
				if(front->right)
				{
					nodeQueue.push(front->right);
					n_next++;
				}

				nodeQueue.pop();
				n_prev--;

				if(n_prev <= 0)
				{
					n_prev=n_next;
					n_next=0;
					ans.push_back(level);
					level.clear();
				}
			}

			reverse(ans.begin(), ans.end());
			return ans;
		}
};
