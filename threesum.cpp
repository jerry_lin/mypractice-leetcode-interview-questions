#include<iostream>
#include<algorithm>
#include<vector>

using namespace std;

class Solution {
	public:
		vector<vector<int> > threeSum(vector<int> &num) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int len=num.size();

			vector< vector <int> > ans;
			if(len<3) return ans;        

			vector<int> tuple(3,-1);

			sort(num.begin(), num.end());

			int prev=num[0]-1;
			for(int i=0; i<len-2; i++)
			{
				int num1=num[i];
				if(prev==num1) continue;
				prev=num1;

				if(num1>0) break;

				int p1=i+1, p2=len-1;
				while(p1<p2)
				{
					int sum=num[p1]+num[p2];
					if(sum==-num1)
					{
						tuple[0]=num1;
						tuple[1]=num[p1];
						tuple[2]=num[p2];
						ans.push_back(tuple);

						//cout << i <<" " << p1 << "" << p2 <<endl;

						int tmp_p1=num[p1], tmp_p2=num[p2];

						while(p1<p2 && num[p1]==tmp_p1)
							p1++;
						while(p1<p2 && num[p2]==tmp_p2)
							p2--;
					}
					else if(sum<-num1)
						p1++;
					else
						p2--;
				}
			}
			return ans;
		}
};


int main()
{
	Solution sol;
	//int myints[] = {-5, -1, 2, 3};
	int myints[] = {0,0,0,0};
	vector<int> num (myints, myints + sizeof(myints) / sizeof(int));

	sol.threeSum(num);

	return 0;
}
