#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Solution {
	public:
		vector<string> generateParenthesis(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			vector<string> res;
			string s="";
			if(n>0) parenthesisRec(0, 0, res, s, n);
			return res;
		}

		void parenthesisRec(int left, int right, vector<string> &res, string s, int n)
		{
			if(left==n && right==n)
			{
				res.push_back(s);
				return;
			}

			if(left<n)
				parenthesisRec(left+1, right, res, s+'(', n);

			if(left>right)
				parenthesisRec(left, right+1, res, s+')', n);    
		}
};


int main()
{
	Solution sol;
	sol.generateParenthesis(2);

	return 0;
}
