/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		bool isValidBST(TreeNode *root) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int low=INT_MIN, high=INT_MAX;
			return isValidBST_Rec(root, low, high);
		}

		bool isValidBST_Rec(TreeNode *root, int low, int high)
		{
			if(!root) return true;
			if(root->val>=high || root->val<=low)
				return false;
			return isValidBST_Rec(root->left, low, root->val)&&isValidBST_Rec(root->right, root->val, high);
		}
};
