
int mapIndex[256];  
void mapToIndices(int inorder[], int n)  
{  
	int i;  
	for (i=0; i<n; i++) {  
		mapIndex[inorder[i]] = i;  
	}  
}  
//.......mapToIndices...pre..................pre.........n......offset........  
struct node* buildInorderPreorder(int pre[],  
		int n, int offset)  
{  
	if (n == 0) return NULL;  
	int rootVal = pre[0];  
	int i = mapIndex[rootVal] - offset;  
	struct node* root = newNode(rootVal);  
	root->left = buildInorderPreorder(pre+1,  
			i, offset);  
	root->right = buildInorderPreorder(pre+i+1,  
			n-i-1, offset+i+1);  
	return root;  
}  
//....  
void buildInorderPreorderTest()  
{  
	int pre[] = {7, 10, 4, 3, 1, 2, 8, 11};  
	int in[] = {4, 10, 3, 1, 7, 11, 8, 2};  
	int n = sizeof(in) / sizeof(in[0]);  
	int offset = 0;  
	mapToIndices(in, n);  
	struct node* root = buildInorderPreorder(pre, n, offset);  
	traverse(root);  
	putchar('\n');  
}  
