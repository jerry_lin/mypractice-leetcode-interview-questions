
class Solution {
	public:
		int searchInsert(int A[], int n, int target) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(n==0) return 0;

			int low=0, high=n-1, mid;

			while(low<=high)
			{
				mid=low+(high-low)/2;

				if(A[mid]==target)
					return mid;
				else if(A[mid]>target)
					high=mid-1;
				else
					low=mid+1;
			}

			if(A[mid]>target) return mid;
			else return mid+1;
		}
};
