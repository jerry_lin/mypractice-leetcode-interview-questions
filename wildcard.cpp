#include<iostream>
#include<vector>

using namespace std;

//O(P) space, O(PS) time
class Solution {
	public:
		bool isMatch(const char *s, const char *p) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(s==NULL || p==NULL) return false;

			int sLen=strlen(s), pLen=strlen(p);

			//handle the last test case: s=aa; p=*aaaa......aaa*
			const char *p2=p;
			int nonStar=0;
			while(*p2){if(*p2!='*') nonStar++; p2++;}
			if(nonStar>sLen) return false;

			vector<bool> canMatch0(pLen+1, false);
			vector<vector<bool> > canMatch(2, canMatch0);

			canMatch[0][0]=true;
			for(int j=0; j<pLen; j++)
				if(p[j]=='*') canMatch[0][j+1]=true;
				else break;

			int curr=0, prev=0;
			for(int i=1; i<=sLen; i++)
			{
				prev=curr;
				curr=(curr+1)%2;

				canMatch[curr][0]=false;

				for(int j=1; j<=pLen; j++)
				{
					if(p[j-1]=='*')
						canMatch[curr][j]=canMatch[prev][j]
							|| canMatch[curr][j-1];
					else if(p[j-1]=='?' || p[j-1]==s[i-1])
						canMatch[curr][j]=canMatch[prev][j-1];
					else
						canMatch[curr][j]=false;
				}
				//optimization if last one is *, and it matches
				if(p[pLen-1]=='*' && canMatch[curr][pLen]) 
					return true;
			}
			return canMatch[curr][pLen];
		}
};

class Solution_Rec {
	public:
		bool isMatch(const char *s, const char *p) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(s==NULL || p==NULL) return false;

			return isMatch_Rec(s, 0, p, 0);
		}

		bool isMatch_Rec(const char *s, int pos1, const char *p, int pos2)
		{
			if(p[pos2]=='\0') return s[pos1]=='\0';

			if(s[pos1]=='\0') //return isMatch_Rec(s, pos1, p, pos2+1);
			{
				while(p[pos2]!='\0')
				{
					if(p[pos2]!='*') return false;
					pos2++;
				}
				return true;
			}

			if(p[pos2]=='*')
				return (isMatch_Rec(s, pos1+1, p, pos2) 
						|| isMatch_Rec(s, pos1, p, pos2+1)
						|| isMatch_Rec(s, pos1+1, p, pos2+1) );
			else if(p[pos2]=='?' || p[pos2]==s[pos1])
				return isMatch_Rec(s, pos1+1, p, pos2+1); 
			else
				return false;            
		}
};


int main()
{
	Solution sol;
	char a[]="hello";
	char b[]="*o";
	bool ans=sol.isMatch(a,b);
	cout<< ans << endl;
	return 0;
}
