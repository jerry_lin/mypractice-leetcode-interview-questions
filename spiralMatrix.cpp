
class Solution {
	public:
		vector<int> spiralOrder(vector<vector<int> > &matrix) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			vector<int> ans;
			int m=matrix.size();
			if(m==0) return ans;

			int n=matrix[0].size();

			spiralOrder_rec(matrix, 0, 0, m, n, ans);
			return ans;
		}

		void spiralOrder_rec(vector<vector<int> > &matrix, int x, int y,
				int row, int col, vector<int> &ans)
		{
			if(row<=0||col<=0) return;

			if(row==1)
			{
				for(int i=y; i<y+col; i++)
					ans.push_back(matrix[x][i]);
				return;
			}
			if(col==1)
			{
				for(int i=x; i<x+row; i++)
					ans.push_back(matrix[i][y]);
				return;
			}

			int row1=x, row2=row1+row-1, col1=y, col2=col1+col-1;

			for(int i=col1; i<col2; i++)
				ans.push_back(matrix[row1][i]);
			for(int i=row1; i<row2; i++)
				ans.push_back(matrix[i][col2]);
			for(int i=col2; i>col1; i--)
				ans.push_back(matrix[row2][i]);
			for(int i=row2; i>row1; i--)
				ans.push_back(matrix[i][col1]);

			spiralOrder_rec(matrix, x+1, y+1, row-2, col-2, ans);
		}
};
