
class Solution {
	public:
		int uniquePaths(int m, int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(m==0 || n==0) return 0;

			vector<int> count(n,1);

			for(int i=1;i<m;i++)
				for(int j=1;j<n;j++)
					count[j] += count[j-1];
			return count[n-1];
		}
};

class Solution_rec {
	public:
		int uniquePaths(int m, int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if(m==0 || n==0) return 0;

			int count=0;

			uniquePaths(m,n,0,0,count);

			return count;
		}

		void uniquePaths(int m, int n, int x, int y, int &count)
		{
			if(x>=m || y>=n) return;

			if(x==m-1 && y==n-1) {count++; return;}

			uniquePaths(m,n,x+1,y,count);
			uniquePaths(m,n,x,y+1,count);
		}
};
