#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
	public:
		int minDistance(string word1, string word2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int n1=word1.size(), n2=word2.size();

			if(n1*n2==0) return (n1+n2);

			vector<int> dist0(n2+1, -1);
			vector<vector<int> > dist(2, dist0);

			for(int i=0; i<=n2; i++)
				dist[0][i]=i;

			int prev=0, curr=0;
			for(int i=1; i<=n1; i++)
			{
				prev=curr;
				curr=(prev+1)%2;
				dist[curr][0]=i;

				for(int j=1; j<=n2; j++)
				{
					if(word1[i-1]==word2[j-1]) 
						dist[curr][j]=dist[prev][j-1];
					else
						dist[curr][j]= 1+min( min(dist[curr][j-1], dist[prev][j]), 
																	dist[prev][j-1]);
				}
			}   
			return dist[curr][n2];
		}
};


class Solution_Rec {
	public:
		int minDistance(string word1, string word2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			return minDistRec(word1, word1.size()-1, word2, word2.size()-1);
		}

		int minDistRec(string &word1, int pos1, string &word2, int pos2)
		{
			if(pos1==-1) return pos2+1;
			if(pos2==-1) return pos1+1;

			if(word1[pos1]==word2[pos2])
				return minDistRec(word1, pos1-1, word2, pos2-1);
			else
				return min( minDistRec(word1, pos1-1, word2, pos2-1)+1,
						min(minDistRec(word1, pos1, word2, pos2-1)+1,
							minDistRec(word1, pos1-1, word2, pos2)+1)
						);
		}
};

int main()
{
	Solution sol;

	string a="sea";
	string b="ate";

	sol.minDistance(a, b);

	return 0;
}
