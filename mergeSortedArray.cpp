
class Solution {
	public:
		void merge(int A[], int m, int B[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int idx1=m-1, idx2=n-1, idx=m+n-1;

			while(idx1>=0 && idx2>=0)
			{
				if(A[idx1]>=B[idx2]) {A[idx]=A[idx1]; idx1--;}
				else {A[idx]=B[idx2]; idx2--;}

				idx--;
			}

			if(idx2>=0)
				for(int i=0; i<=idx2; i++)
					A[i]=B[i];
		}
};
