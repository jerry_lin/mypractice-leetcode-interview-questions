
class Solution {
	public:
		void rotate(vector<vector<int> > &matrix) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			int len=matrix.size();

			for(int i=0; i<len/2; i++)
			{
				int row1=i, row2=len-1-i, col1=len-1-i, col2=i;

				for(int j=i; j<len-1-i; j++)
				{
					int tmp=matrix[row1][j];
					matrix[row1][j]=matrix[len-1-j][col2];
					matrix[len-1-j][col2]=matrix[row2][len-1-j];
					matrix[row2][len-1-j]=matrix[j][col1];
					matrix[j][col1]=tmp;
				}

			}
		}
};
