/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
	public:

		static bool mycompare(const Interval &a, const Interval &b)
		{
			return (a.start<b.start);
		}

		vector<Interval> merge(vector<Interval> &intervals) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(intervals.size()<=1) return intervals;

			vector<Interval> ans;
			Interval tmp;

			sort(intervals.begin(), intervals.end(), Solution::mycompare);

			ans.push_back(intervals[0]);
			for(int i=1; i<intervals.size(); i++)
			{
				if(intervals[i].start <= ans.back().end)
					ans.back().end=max(intervals[i].end, ans.back().end);
				else
					ans.push_back(intervals[i]);
			}

			return ans;
		}
};
