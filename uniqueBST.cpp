
class Solution {
	public:
		int numTrees(int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			//This is Catalan number
			vector<int> catalan(n+1, 1);

			for(int i=2; i<=n; i++)
			{
				int sum=0;
				for(int j=0; j<=i-1; j++)
				{
					sum+=catalan[j]*catalan[i-1-j];
				}
				catalan[i]=sum;
			}
			return catalan[n];
		}
};
