/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
// #include<priority_queue>
class Solution {
	public:

		class compare
		{
			public:
				bool operator()(const ListNode *a, const ListNode *b)
				{
					return (a->val > b->val);
				}
		};

		ListNode *mergeKLists(vector<ListNode *> &lists) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			ListNode *ans, *curr;
			int sz = lists.size();
			if(sz==0) return NULL;

			priority_queue<ListNode*, vector<ListNode*>, compare> myheap;

			for(int i=0; i<sz; i++)
			{
				if(lists[i]!=NULL)
					myheap.push(lists[i]);
			}

			if(myheap.empty()) return NULL;
			ans=myheap.top();
			myheap.pop();
			curr=ans;        
			if(curr->next) myheap.push(curr->next);

			while(!myheap.empty())
			{
				curr->next=myheap.top();
				myheap.pop();
				curr=curr->next;
				if(curr->next) myheap.push(curr->next);
			}

			return ans;
		}
};
