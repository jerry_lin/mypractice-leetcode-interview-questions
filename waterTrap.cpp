#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
#include<stack>

using namespace std;



class Solution {
	public:
		int trap(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			if (!A || !n) return 0;

			int highest = 0, water = 0, h = 0;
			for (int i = 0; i < n; ++i) {
				if (A[i] > A[highest])
					highest = i;
			}

			for (int i = 0; i < highest; ++i) {
				if (h > A[i])
					water += h - A[i];
				else
					h = A[i];
			}

			h = 0;
			for (int i = n - 1; i > highest; --i) {
				if (h > A[i])
					water += h - A[i];
				else
					h = A[i];
			}
			return water;
		}
};


class Solution_stack {
	public:
		int trap(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(n<=2) return 0;

			stack<int> st;
			int height=0, leftmost, rightmost;
			int sum=0;

			st.push(0);
			int i=1;
			while(i<n)
			{
				if(A[i]<A[st.top()])
					st.push(i);
				else 
				{
					height=A[st.top()];

					while(!st.empty() && A[i]>=A[st.top()])
					{
						height=max(height, A[st.top()]);
						leftmost = st.top();
						st.pop();
					}

					if(st.empty())
					{
						for(int j=leftmost+1; j<i; j++)
							sum += (height-A[j]);
					}
					st.push(i);                
				}
				i++;
			}

			if(!st.empty())
			{
				rightmost=st.top();
				st.pop();
				while(!st.empty())
				{
					leftmost = st.top();
					st.pop();
					for(int j=leftmost+1; j<rightmost; j++)
						sum += (A[rightmost]-A[j]);
					rightmost=leftmost;
				}
			}

			return sum;
		}
};


int main()
{
	Solution sol;

	return 0;
}
