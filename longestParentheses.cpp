
class Solution {
	public:
		int longestValidParentheses(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len=s.size();
			if(len==0) return 0;

			int longest=0;
			stack<pair<char, int> > st;
			st.push(pair<char, int>('*', 0));

			for(int i=0; i<len; i++)
			{
				if(s[i]=='(')
					st.push(pair<char, int>('(', 0));
				else
				{
					if(st.top().first=='*')
						st.top().second=0;
					else // '('
					{
						int prev=st.top().second;
						st.pop();
						st.top().second +=(prev+2);
						longest = max(longest, st.top().second);
					}
				}
			}
			return longest;
		}
};

//a version without to store '(' and ')'
class Solution2 {
	public:
		int longestValidParentheses(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len=s.size();
			if(len==0) return 0;

			int longest=0;
			stack< int > st;
			int fromStart=0;

			for(int i=0; i<len; i++)
			{
				if(s[i]=='(')
					st.push(0);
				else //'(' 
				{
					if(st.empty())
						fromStart=0;
					else
					{
						int tmp1=st.top();
						st.pop();
						if(st.empty())
						{
							fromStart += tmp1+2;
							longest=max(longest, fromStart);
						}
						else
						{
							int tmp2=st.top();
							st.pop();
							tmp2 += tmp1+2;
							st.push(tmp2);
							longest=max(longest, tmp2);
						}
					}
				}
			}
			return longest;
		}
};
