/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
	public:
		ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			ListNode *head, *tail;

			head=tail=new ListNode(0);
			int carry=0;
			while( l1 && l2 )
			{
				int sum=l1->val+l2->val+carry;
				if(sum>9)
				{
					carry=1;
					sum-=10;
				}
				else
					carry=0;


