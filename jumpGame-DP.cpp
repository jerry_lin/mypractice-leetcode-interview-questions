class Solution {
	public:
		bool canJump(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			bool *canReach = new bool[n];

			for(int i=0; i<n; i++) canReach[i]=false;
			canReach[n-1]=true;

			for(int i=n-2; i>=0; i--)
			{
				for(int j=1; j<=A[i]; j++)
				{
					if(i+j>=n) break;

					if(canReach[i+j])
					{
						canReach[i]=true;
						break;
					}
				}

			}

			bool ans=canReach[0];
			delete canReach;
			return ans;
		}
};
