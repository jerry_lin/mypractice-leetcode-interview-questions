class Solution {
	public:
		int firstMissingPositive(int A[], int n) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			for(int i=0; i<n; i++)
			{
				while(A[i]>=0 && A[i]<n && A[i]!=i && A[i]!=A[A[i]])
				{
					int tmp=A[A[i]];
					A[A[i]]=A[i];
					A[i]=tmp;
				}
			}

			for(int i=1; i<n; i++)
			{
				if(A[i]!=i) return i; 
			}

			if(A[0]!=n) return n;
			return n+1;
		}
};
