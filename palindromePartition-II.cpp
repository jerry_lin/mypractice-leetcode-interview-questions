
class Solution {
	public:
		int minCut(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int len = s.size();
			if(len==0) return 0;

			vector<bool> each(len, false);
			vector<vector<bool> > isPalindrom(len, each);

			for(int i=0; i<len; i++) isPalindrom[i][i]=true;

			vector<int> dpMin(len, 0);
			dpMin[0]=0;

			for(int i=1; i<len; i++)
			{   
				int minLen=dpMin[i-1]+1;
				for(int j=i-1; j>=0; j--)
				{
					if(s[i]==s[j] && 
							(j+1>i-1 || isPalindrom[j+1][i-1]) )
					{
						isPalindrom[j][i]=true;
						if(j==0) minLen=0;
						else minLen=min(minLen, 1+dpMin[j-1]);
					}
					else
						isPalindrom[j][i]=false;
				}
				dpMin[i]=minLen;
			}

			return dpMin[len-1];
		}
};
