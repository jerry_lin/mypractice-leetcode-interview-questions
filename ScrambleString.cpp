class Solution {
	public:
		bool isScramble(string s1, string s2) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function
			int n = s1.length(), n2 = s2.length();
			if(n!=n2) return false;
			if(n==1)
				return (s1==s2);

			for(int i=1; i<n; i++)
			{
				if(isScramble(s1.substr(0,i), s2.substr(0,i))  
						&&  isScramble(s1.substr(i, n-i), s2.substr(i, n-i)) )            
					return true;

				if(isScramble(s1.substr(0,i), s2.substr(n-i, i) )
						&& isScramble(s1.substr(i,n-i), s2.substr(0, n-i)) )
					return true;
			}

			return false;
		}
};
