
class Solution {
	public:
		int romanToInt(string s) {
			// Start typing your C/C++ solution below
			// DO NOT write int main() function

			if(s.size()==0) return 0;
			int prev=translate(s[0]);
			int res=prev;

			for(int i=1; i<s.size(); i++)
			{
				int tmp=translate(s[i]);
				if(tmp>prev)
					res -= 2*prev;
				res += tmp;
				prev=tmp;
			}

			return res;
		}

		int translate (char c) {
			switch(c){
				case 'I': return 1;
				case 'V': return 5;
				case 'X': return 10;
				case 'L': return 50;
				case 'C': return 100;
				case 'D': return 500;
				case 'M': return 1000;
			}
			return 0;
		}
};
